const express = require('express');
const router =  express.Router();
const path = require('path');
const mongoose = require('mongoose');

//Dependencia para cargar archivo en este caso la foto en la carpeta uploads
const multer = require('multer');

/* const upload = multer({ dest: 'uploads/'}) */


//Almacenar imagen
const storage = multer.diskStorage({
    destination:'./public/uploads/',
    filename : function(req ,file, cb){
        cb(null, file.fieldname +'-'+ Date.now() + 
        path.extname(file.originalname));
    }
});

//init upload
const upload = multer({
    storage : storage
}).single('foto');

//Dependencia para leer la metadata de la imagen
const ExifImage = require('exif')

//traer el modelo de la base de datos
const Fotos = require('../model/foto');

 



router.get('/', function (req, res) {
    res.send('Hello World!');
  });

router.get('/datosdb', async function(req,res){
    //Consulta base de datos
    const fotos_find = await Fotos.find()
    res.send(fotos_find);
});

router.get('/data', async(req,res) => {
        res.render('results');
});


router.post('/foto', async function (req, res) {

    await upload(req, res, (err) => {

 //Creamos el objeto a insertar en la base de datos
        foto_info = {
            title: req.file.fieldname,
            name: req.file.originalname,
            size: req.file.size,
            path: req.file.path,
        }
        if(err){
            res.send(err)
        }else{
            //ruta de la imagen
            var ruta = `public/uploads/${req.file.filename}`;

                new ExifImage({ image : ruta }, function (error, exifData) {
                    if (error){
                        console.log('Error: '+error.message);
                    }
                    else{
                        console.log(exifData);  // Do something with your data!
                        //create object to send

                        const foto = new Fotos(foto_info)
                        //Insertamos el objeto en la base de datos
                        foto.save();
            
                        //Renderizamos la vista
                        res.render('foto_data',{
                            file: `uploads/${req.file.filename}`,
                            marca: exifData.image.Make,
                            modelo: exifData.image.Model,
                            fecha: exifData.exif.DateTimeOriginal,
                            latitud: exifData.gps.GPSLatitude,
                            longitud: exifData.gps.GPSLongitude
                        }); 
                    }
                });
        }
    })
           


            





  })
module.exports = router;