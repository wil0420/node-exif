
const path = require('path');
const express = require('express');
const ejs = require('ejs');
const mongoose = require('mongoose');

const app = express();

//conexion base de datos
mongoose.connect('mongodb://localhost/dbfotos')
    .then(db => console.log('Db Conectada'))
    .catch(db => console.log(err));


//Rutas 
const indexRoutes = require('./routes/routes');


//Ruta de la carpeta publica de archivos estaticos
app.use(express.static(path.join(__dirname,'views')));


//Rutas de las vistas predeterminada
app.set('views', path.join(__dirname, 'views'));

//Uso de archivos estaticos
app.use(express.static('./public'));

//definir motor de plantilla
app.set('view engine', 'ejs');

app.use(express.urlencoded({extended:false}));

app.use('/', indexRoutes);


app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});