const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const FotoSchema = new Schema({
    title: String,
    name: String,
    size: Number,
    path: String,

});

//Exportamos el modelo de datos 
 module.exports =  mongoose.model('fotos',FotoSchema)